/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AI_FIRE_START = 3323207283U;
        static const AkUniqueID AI_FIRE_STOP = 191632305U;
        static const AkUniqueID AMBIENT_OCEAN_SOUND = 2277341620U;
        static const AkUniqueID AMBIENT_THUNDER = 3719894632U;
        static const AkUniqueID AMBIENT_WATHER_CITY = 1926011535U;
        static const AkUniqueID AMBIENT_WEATHER = 1152562982U;
        static const AkUniqueID AMBIENT_WIND_TREES = 4121333870U;
        static const AkUniqueID CHARACTER_FIRE_START = 1172748428U;
        static const AkUniqueID CHARACTER_FIRE_STOP = 2633222176U;
        static const AkUniqueID CHARACTER_FOOTSTEPS = 2775932802U;
        static const AkUniqueID CHARACTER_JUMPING = 3057570747U;
        static const AkUniqueID CHARACTER_LANDING = 4274694430U;
        static const AkUniqueID CHARACTER_PPSH = 2068700040U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace GUN
        {
            static const AkUniqueID GROUP = 915214409U;

            namespace SWITCH
            {
                static const AkUniqueID COCKING_HANDLE = 896109900U;
                static const AkUniqueID DRY_FIRE = 1682435867U;
                static const AkUniqueID FIRE_START = 1712046992U;
                static const AkUniqueID FIRE_STOP = 1981293180U;
                static const AkUniqueID MAG_IN = 4041765320U;
                static const AkUniqueID MAG_OUT = 485474127U;
                static const AkUniqueID SAFETY = 374946131U;
            } // namespace SWITCH
        } // namespace GUN

        namespace MOVE_TYPE
        {
            static const AkUniqueID GROUP = 1597344961U;

            namespace SWITCH
            {
                static const AkUniqueID RUNNING = 3863236874U;
                static const AkUniqueID SPRINTING = 3691594375U;
            } // namespace SWITCH
        } // namespace MOVE_TYPE

        namespace SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 4064446173U;

            namespace SWITCH
            {
                static const AkUniqueID ASPHALT = 4169408098U;
                static const AkUniqueID DIRT = 2195636714U;
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID METAL = 2473969246U;
                static const AkUniqueID STONE = 1216965916U;
                static const AkUniqueID WATER = 2654748154U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace SURFACE_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID RTPC_EXT_CAMERA_HEIGHT = 566631840U;
        static const AkUniqueID RTPC_EXT_WEATHER = 330698749U;
        static const AkUniqueID RTPC_INT_AZIMUTH = 406513959U;
        static const AkUniqueID RTPC_INT_DISTANCE = 3025684452U;
        static const AkUniqueID RTPC_OCCLUSION = 1664992182U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTERBANK = 1542108333U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID CHARACTER_LOCOMOTION = 3022164222U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
