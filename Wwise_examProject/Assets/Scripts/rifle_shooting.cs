﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using Invector.vShooter;

public class rifle_shooting : MonoBehaviour
{
    public vShooterMeleeInput shooter;

    public vShooterWeapon weapon;

    public AK.Wwise.Event shotStart;
    public AK.Wwise.Event shotStop;

    protected vShooterManager ShooterManager;
    // Start is called before the first frame update
    void Start()
    {
        ShooterManager = GetComponent<vShooterManager>();
        shooter = GetComponent<vShooterMeleeInput>();
               
    }

    // Update is called once per frame
    void Update()
    {
        if (ShooterManager.rWeapon.ammoCount > 0)
        {
            if (Input.GetKey(KeyCode.Mouse1))
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    shotStart.Post(gameObject);
                }
                else if (Input.GetKeyUp(KeyCode.Mouse0))
                {
                    shotStop.Post(gameObject);
                }
            }
        }
    }
}
