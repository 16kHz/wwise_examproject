﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;

public class AI_footsteps : MonoBehaviour
{
    public AK.Wwise.Event footeventAI;
    public AK.Wwise.Event landingeventAI;
    public AK.Wwise.Event jumpingeventAI;
    public LayerMask lmAI;
    public vIControlAIShooter AI;
    // Start is called before the first frame update
    void Start()
    {
        AI = GetComponent<vIControlAIShooter>();
    }

    // Update is called once per frame
    void footstep_walking()
    {
        if (AI.isMoving == true) //создаём условие, при котором шаги будут воспроизводиться когда контроллер будет двигаться
        {
            if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, 3f, lmAI))
            {
                AkSoundEngine.SetSwitch("surface_type", hit.collider.tag, gameObject);
                AkSoundEngine.SetSwitch("move_type", "running", gameObject);
                AkSoundEngine.SetSwitch("footstepLR", "footstep_L", gameObject);
                footeventAI.Post(gameObject);
            }
        }
    }
    void landing()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, 0.3f, lmAI))
        {
            AkSoundEngine.SetSwitch("surface_type", hit.collider.tag, gameObject);
            landingeventAI.Post(gameObject);
        }
        if (lmAI == default)
        {
            landingeventAI.Post(gameObject);
        }
    }


    void jumping()
    {
        jumpingeventAI.Post(gameObject);
    }
}
