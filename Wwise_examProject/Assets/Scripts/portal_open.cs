﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portal_open : MonoBehaviour
{
    public GameObject portal;
    private AkRoomPortal AKPortal;
    // Start is called before the first frame update
    void Start()
    {
        AKPortal = portal.GetComponent<AkRoomPortal>();
    }

    // Update is called once per frame
    public void PortalOpen()
    {
        AKPortal.Open();
    }
    public void PortalClose()
    {
        AKPortal.Close();
    }
}
