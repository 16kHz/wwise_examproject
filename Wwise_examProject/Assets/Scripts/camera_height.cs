﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_height : MonoBehaviour
{
    public float cam;

    // Start is called before the first frame update

    void Update()
    {
        cam = gameObject.transform.position.y;
        AkSoundEngine.SetRTPCValue("RTPC_ext_camera_height", cam);
    }
}
