﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weather : MonoBehaviour
{
    public float rainTime;
    public float freq = 0.1f;
    public bool peak = false;

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (rainTime >= 99)
        {
            peak = true;
        }
        else if (rainTime <= 1)
        {
            peak = false;
        }
        if (peak == false)
        {
            rainTime = rainTime + freq;
        }
        else if (peak == true)
        {
            rainTime = rainTime - freq;
        }
        AkSoundEngine.SetRTPCValue("RTPC_ext_weather", rainTime);
    }
}
