﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character_jumping : MonoBehaviour
{
    public AK.Wwise.Event landingEvent;
    public AK.Wwise.Event jumpingEvent;
    public LayerMask lm;

    void landing()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 3f, lm))
        {
            AkSoundEngine.SetSwitch("surface_type", hit.collider.tag, gameObject);
            landingEvent.Post(gameObject);
        }
    }
    void jumping()
    {
        jumpingEvent.Post(gameObject);
    }
}
