﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class character_footsteps : MonoBehaviour
{
    public GameObject footLeft;
    public GameObject footRight;
    public AK.Wwise.Event footevent;
    public LayerMask lm;
    public vShooterMeleeInput character;

    private void Start()
    {
        character = GetComponent<vShooterMeleeInput>();
    }
    void footstep_walking(string arg)
    {
        if (arg == "left")
        {
            Playfootstep(footLeft);
        }
        else if (arg == "right")
        {
            Playfootstep(footRight);
        }
    }

    void Playfootstep(GameObject footObject)
    {
        if (character.cc.isSprinting)
        {
            AkSoundEngine.SetSwitch("move_type", "sprinting", footObject);
        }
        else
        {
            AkSoundEngine.SetSwitch("move_type", "running", footObject);
        }
        if (Physics.Raycast(footObject.transform.position, Vector3.down, out RaycastHit hit, 3f, lm))
        {
            AkSoundEngine.SetSwitch("surface_type", hit.collider.tag, footObject);
            footevent.Post(footObject);
        }
        else
        {
            footevent.Post(footObject);
        }
    }
}

