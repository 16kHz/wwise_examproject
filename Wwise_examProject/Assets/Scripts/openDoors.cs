﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openDoors : MonoBehaviour
{
    Animator anim;
    bool openDoor = false;
    public bool PlayerInTrigger = false;
    public AK.Wwise.Event doorEvent;
    public GameObject doors;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerInTrigger = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        PlayerInTrigger = false;
    }
    void DoorControl(string direction)
    {
        anim.SetTrigger(direction);
    }
    // Update is called once per frame
    void Update()
    {
        if (PlayerInTrigger && !openDoor && Input.GetKeyDown("e"))
        {
            openDoor = true;
            DoorControl("isOpen");
        }
        else if (PlayerInTrigger && openDoor && Input.GetKeyDown("e"))
        {
            DoorControl("isClose");
            openDoor = false;
        }
    }
        void playOpenSound()
        {
            AkSoundEngine.SetSwitch("doors", "open", doors);
            doorEvent.Post(doors);
        }
        void playCloseSound()
        {
             AkSoundEngine.SetSwitch("doors", "close", doors);
             doorEvent.Post(doors);
        }
}
